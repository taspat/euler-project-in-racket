#lang racket
(require math/number-theory)

(define (f p i)
  (if (= i 10001)
      p
      (f (next-prime p) (+ 1 i))))

(f 2 1)
