#lang racket

(define (f x)
  (define (g y z)
    (cond [(>= z y) y]
          [(zero? (remainder y z)) (g (/ y z) z)]
          [else (g y (+ 1 z))]))
  (g x 2))
(f 600851475143)
