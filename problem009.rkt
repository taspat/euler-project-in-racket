#lang racket

(for*/first ([x (range 1 500)]
             [y (range 2 500)]
             [z (range 3 500)]
             #:when (and (= (+ x y z) 1000)
                         (= (* z z) (+ (* x x) (* y y)))))
  (* x y z))

