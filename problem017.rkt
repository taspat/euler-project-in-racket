#lang racket

(define a
  (list "one" "two" "three" "four" "five" "six" "seven" "eight" "nine"))
(define b
  (list "ten" "eleven" "twelve" "thirteen" "fourteen" "fifteen" "sixteen" "seventeen" "eighteen" "nineteen"))
(define c
  (list "twenty" "thirty" "forty" "fifty" "sixty" "seventy" "eighty" "ninety"))
(define d "hundred")
(define e "and")
(define f
  (list "one" "thousand"))

(+ (* 190 (apply + (map string-length a)))
   (* 10 (apply + (map string-length b)))
   (* 100 (apply + (map string-length c)))
   (* 900 (string-length d))
   (* 891 (string-length e))
   (apply + (map string-length f)))
